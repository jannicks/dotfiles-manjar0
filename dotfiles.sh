#!/bin/bash

#
# 13-02-2019, J. Sikkema, Dotfiles tool that manages user (awesome) dotfiles from a single distributed source
#

SOURCE_DIR="/opt/dotfiles"
DEST_DIR="$HOME/.dotfiles"
BACKUP_DIR="$HOME/.backup_dotfiles"
DOTFILE_RC="$HOME/.dotfilerc"
PRESERVE_PROFILE_VARS='true'
PRESERVE_GIT_USER='true'

git_source () {
    if [[ -z ${GIT_URL} ]]; then
        return 0
    fi
    GIT_DIR=`basename $(echo ${GIT_URL%.git}) 2> /dev/null`
    if [[ ${?} != '0' ]] || [[ -z ${GIT_DIR} ]]; then
        echo "Syntax error while parsing repo name from git url: ${GUT_URL}"
        return 1
    fi
    if [[ "$GIT_DIR" == 'dotfiles' ]]; then
        GIT_DIR="git-$GIT_DIR"
    fi
    if  [[ ! -d "${HOME}/${GIT_DIR}" ]]; then
        cd ${HOME}
        git clone ${GIT_URL} ${GIT_DIR}
        if [[ ${?} != '0' ]]; then
            echo "Error encountered while attempting to clone git repository:"
            printf "${GIT_URL} in ${GIT_DIR}"
            return 1
        fi
    else
        if [[ ${1} == 'upgrade:true' ]]; then
            cd "${HOME}/${GIT_DIR}"
            git pull
            if [[ ${?} != '0' ]]; then
                echo "Error encountered while trying to update local git directory in:"
                printf "${HOME}/${GIT_DIR}"
                return 1
            fi
        fi
    fi
    LOCATE_DIR=$(find ${HOME}/${GIT_DIR} -type d -name 'dotfiles')
    if [[ ! -d ${LOCATE_DIR} ]]; then
        echo "Error, cannot find dotfiles directory in: ${HOME}/${GIT_DIR}"
        return 1
    else
        SOURCE_DIR=${LOCATE_DIR}
    fi
}

preserve_profile () {
    if [[ ${PRESERVE_PROFILE_VARS} == 'true' ]]; then
        orig_profile=$(find ${BACKUP_DIR} -name '.bash_profile' 2> /dev/null | head -1)
        if [[ ! -f ${orig_profile} ]]; then
            return 0
        fi
        IFS=$'\n'
        for var in $(grep -E ".*=.*|export" ${orig_profile}); do
            if [[ `grep -F ${var} "${DEST_DIR}/bash/.bash_profile"` ]]; then
                continue
            else
                echo ${var} >> "${DEST_DIR}/bash/.bash_profile"
            fi
        done
    else
        return 0
    fi
}

preserve_git_conf () {
    if [[ ${PRESERVE_GIT_USER} == 'true' ]]; then
        orig_gitconfig=$(find ${BACKUP_DIR} -name '.gitconfig' 2> /dev/null | head -1)
        if [[ ! -f ${orig_gitconfig} ]] && [[ -f "${DEST_DIR}/git/.gitconfig" ]]; then
            if [[ $(grep -F "[user]" ${DEST_DIR}/git/.gitconfig) ]]; then
                cp "${DEST_DIR}/git/.gitconfig" "${BACKUP_DIR}/.gitconfig"
            fi
        fi
        orig_gitconfig="${BACKUP_DIR}/.gitconfig"
        if [[ ! -f ${orig_gitconfig} ]]; then
            return 0
        fi
        if [[ $(grep -Fvo "[user]" ${orig_gitconfig}) ]]; then
            return 0
        fi
        if [[ $(find ${HOME} -maxdepth 1 -name '.gitconfig' -exec grep -F '[user]' {} \;) ]]; then
            return 0
        fi
        if [[ $(grep -F "[user]" ${orig_gitconfig}) ]] && [[ -f "${DEST_DIR}/git/.gitconfig" ]]; then
            grep -F "[user]" ${orig_gitconfig} >> "${DEST_DIR}/git/.gitconfig"
            grep -F "name = " ${orig_gitconfig} >> "${DEST_DIR}/git/.gitconfig"
            grep -F "email = " ${orig_gitconfig} >> "${DEST_DIR}/git/.gitconfig"
        fi
    else
        return 0
    fi
}

init_dotfiles () {
    echo "Initializing dotfiles..."
    validate_bins stow
    validate_dirs "$SOURCE_DIR" "$HOME"
    mkdir -p "$DEST_DIR"
    check_relative_path
    cp -ru "$SOURCE_DIR"/. -t "$DEST_DIR"
    backup_dotfiles
    preserve_profile
    preserve_git_conf
    cd "$DEST_DIR"
    stow *
    echo "Dotfiles initilazation done"
}

restore_dotfiles () {
    echo "Restoring dotfiles from local backup directory"
    validate_dirs "$BACKUP_DIR" "$HOME"
    if [[ -z `find "$BACKUP_DIR"/. -mindepth 1` ]]; then
        echo "No backups found in $BACKUP_DIR"
        return 1
    fi
    cp --remove-destination -rv $BACKUP_DIR/. $HOME/
    echo "Dotfiles restore done"
}

disable_dotfiles () {
    echo "Disable managed dotfiles..."
    validate_bins stow
    validate_dirs "$DEST_DIR" "$HOME"
    check_relative_path
    backup_dotfiles
    cd "$DEST_DIR"
    stow -D *
    restore_dotfiles
    echo "Dotfiles removal done"
}

upgrade_dotfiles () {
    echo "Upgrading dotfiles from source: $SOURCE_DIR"
    validate_bins stow
    validate_dirs "$DEST_DIR" "$HOME" "$SOURCE_DIR"
    check_relative_path
    backup_dotfiles
    cp -rvu "$SOURCE_DIR"/. -t "$DEST_DIR"
    preserve_profile
    preserve_git_conf
    cd "$DEST_DIR"
    stow -R *
    echo "Dotfiles upgrade done"
}

backup_dotfiles () {
    cd "$DEST_DIR"
    mkdir -p "$BACKUP_DIR"
    for file in `stow -nvv * 2>&1 | grep -F "CONFLICT" | cut -d ':' -f 3`; do
        printf " backup $file to backup directory, "
        mkdir -p `dirname "$BACKUP_DIR/$file"`
        mv -v "$HOME/$file" "$BACKUP_DIR/$file"
    done
}

validate_bins () {
    for bin in "$@"; do
        if [[ ! `command -v ${bin}` ]]; then
            echo "Binary: ${bin} not found!, exiting..."
            exit 1
        fi
    done
}

validate_dirs () {
    for dir in "$@"; do
        if [[ ! -d ${dir} ]]; then
            echo "Directory: ${dir} not found!, exiting..."
            exit 1
        fi
    done
}

check_relative_path () {
    check_path=$(realpath -L ${DEST_DIR} --relative-base=${HOME} | grep -Fov '/')
    if [[ ${?} != '0' ]]; then
        echo "Error: ${DEST_DIR} should always be nested in your home directory on the top level."
        exit 1
    fi
}

show_help () {
echo -e "
This tool manages 'awesome' & improved user dotfiles from a single distributed source.

Run 'dotfiles --init' to use them, amongst its improvements are:
- Syntax highlighting / checking and powerline for vim.
- Handy bash aliases and git enhancements.
- Customized shell prompt with pretty colors.
- 'Smart' bash completion.
- Optional capability: Set custom variables in ~/.extend_bashrc, these will overwrite vars from dotfiles.
- Optional capability: Define a git url in ${DOTFILE_RC} to use your own git repo as source.
  > This file should include a string like: GIT_URL='https://gitlab.com/{User_name}/{Repo_name}.git'

Usage:
dotfiles --init
    Install awesome dotfiles for bash, vim and git from ${SOURCE_DIR} or a git repo defined in ${DOTFILE_RC}.

dotfiles --upgrade
    Upgrade existing awesome dotfiles, syncing latest modifications from ${SOURCE_DIR} or ${DOTFILE_RC}.

dotfiles --restore
    Restoring user dotfiles from ${BACKUP_DIR}, overwriting awesome dotfiles in the process.

dotfiles --disable
    Purge awesome dotfiles and restore backup dotfiles from directory: ${BACKUP_DIR}.

 - Note:
   Within the ~/.dotfilerc file, you can set specific variables to adjust the tool's behaviour to your own preferences:
    SOURCE_DIR=\"~/.source_dotfiles\"   # Set the managed dotfiles directory, defaults to /opt/dotfiles
    DEST_DIR=\"~/.my_dotfiles\"         # Set your own dotfiles directory, defaults to ~/.dotfiles
    BACKUP_DIR=\"~/.my_backups\"        # Set the backup directory, defaults to ~/.backup_dotfiles
    GIT_URL=\"https://git_url.git\"     # Optional: Set a git url to fetch dotfile data from, overrides SOURCE_DIR
    PRESERVE_PROFILE_VARS=\"true\"      # Default is true, set to false to stop preserving preset bash variables from ~/.bash_profile
    PRESERVE_GIT_USER=\"true\"          # Default is true, set to false to stop preserving user specific git config from ~/.gitconfig
    "
}

# Check user opts and source .dotfilerc if present
validate_opts () {
    if [[ -z "$@" ]]; then
        echo "No arguments given, please supply an argument:"
        show_help
        exit 1
    fi
    if [[ ! `echo "$@" | grep -Ei "init|restore|disable|upgrade|help"` ]]; then
        echo "Invalid arguments given, please supply a valid argument"
        show_help
        exit 1
    fi
    if [[ -f ${DOTFILE_RC} ]]; then
        source ${DOTFILE_RC}
    fi
}

# Get shell options and initiate functions.
init_opts () {
    while getopts ":i:r:d:u:h" opt; do
        case $opt in
        i)
            git_source
            init_dotfiles
            unset PS1
            cd && bash
            ;;
        r)
            restore_dotfiles
            unset PS1
            cd && bash
            ;;
        d)
            disable_dotfiles
            unset PS1
            cd && bash
            ;;
        u)
            git_source 'upgrade:true'
            upgrade_dotfiles
            unset PS1
            cd && bash
            ;;
        h)
            show_help
            ;;
        esac
    done
    shift $((OPTIND-1))
    [ "$1" = "--" ] && shift
}

# Execute main function with shell options
main_exec () {
    validate_opts ${@}
    init_opts ${@}
}

main_exec ${@}

